<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => 'c8fa324a4d3817a03f0b110ed739764cf2c9ffd1',
    'name' => 'yiisoft/yii2-app-basic',
  ),
  'versions' => 
  array (
    'behat/gherkin' => 
    array (
      'pretty_version' => 'v4.6.2',
      'version' => '4.6.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '51ac4500c4dc30cbaaabcd2f25694299df666a31',
    ),
    'bower-asset/bootstrap' => 
    array (
      'pretty_version' => 'v3.4.1',
      'version' => '3.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '68b0d231a13201eb14acd3dc84e51543d16e5f7e',
    ),
    'bower-asset/inputmask' => 
    array (
      'pretty_version' => '3.3.11',
      'version' => '3.3.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '5e670ad62f50c738388d4dcec78d2888505ad77b',
    ),
    'bower-asset/jquery' => 
    array (
      'pretty_version' => '3.5.1',
      'version' => '3.5.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '4c0e4becb8263bb5b3e6dadc448d8e7305ef8215',
    ),
    'bower-asset/jquery-ui' => 
    array (
      'pretty_version' => '1.12.1',
      'version' => '1.12.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '44ecf3794cc56b65954cc19737234a3119d036cc',
    ),
    'bower-asset/punycode' => 
    array (
      'pretty_version' => 'v1.3.2',
      'version' => '1.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '38c8d3131a82567bfef18da09f7f4db68c84f8a3',
    ),
    'bower-asset/yii2-pjax' => 
    array (
      'pretty_version' => '2.0.7.1',
      'version' => '2.0.7.1',
      'aliases' => 
      array (
      ),
      'reference' => 'aef7b953107264f00234902a3880eb50dafc48be',
    ),
    'cebe/markdown' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.2.x-dev',
      ),
      'reference' => '2b2461bed9e15305486319ee552bafca75d1cdaa',
    ),
    'codeception/codeception' => 
    array (
      'pretty_version' => '4.1.x-dev',
      'version' => '4.1.9999999.9999999-dev',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => 'bf2d7861cc6cf4e118a21c6945f482a5c822636c',
    ),
    'codeception/lib-asserts' => 
    array (
      'pretty_version' => '1.13.2',
      'version' => '1.13.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '184231d5eab66bc69afd6b9429344d80c67a33b6',
    ),
    'codeception/lib-innerbrowser' => 
    array (
      'pretty_version' => '1.3.5',
      'version' => '1.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '956101b060a0d898a1ed8c106fd4d81adf25fd87',
    ),
    'codeception/module-asserts' => 
    array (
      'pretty_version' => '1.3.1',
      'version' => '1.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '59374f2fef0cabb9e8ddb53277e85cdca74328de',
    ),
    'codeception/module-filesystem' => 
    array (
      'pretty_version' => '1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '781be167fb1557bfc9b61e0a4eac60a32c534ec1',
    ),
    'codeception/module-yii2' => 
    array (
      'pretty_version' => '1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '7e2eaeb414315271d545e17c330b3aaf08911927',
    ),
    'codeception/phpunit-wrapper' => 
    array (
      'pretty_version' => '8.1.4',
      'version' => '8.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f41335f0b4dd17cf7bbc63e87943b3ae72a8bbc3',
    ),
    'codeception/specify' => 
    array (
      'pretty_version' => '0.4.6',
      'version' => '0.4.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '21b586f503ca444aa519dd9cafb32f113a05f286',
    ),
    'codeception/stub' => 
    array (
      'pretty_version' => '3.7.0',
      'version' => '3.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '468dd5fe659f131fc997f5196aad87512f9b1304',
    ),
    'codeception/verify' => 
    array (
      'pretty_version' => '1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fa0bb946b6d61279f461bcc5a677ac0ed5eab9b3',
    ),
    'components/flag-icon-css' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '1dda087cee478cdd4d180dbdbab5a6f2fba9e70a',
    ),
    'doctrine/instantiator' => 
    array (
      'pretty_version' => '1.5.x-dev',
      'version' => '1.5.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '6410c4b8352cb64218641457cef64997e6b784fb',
    ),
    'doctrine/lexer' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.3.x-dev',
      ),
      'reference' => 'cffe3e94d62f717a10a7d8614a19917f9311a5bc',
    ),
    'egulias/email-validator' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.1.x-dev',
      ),
      'reference' => '0dbf5d78455d4d6a41d186da50adc1122ec066f4',
    ),
    'ezyang/htmlpurifier' => 
    array (
      'pretty_version' => 'v4.13.0',
      'version' => '4.13.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '08e27c97e4c6ed02f37c5b2b20488046c8d90d75',
    ),
    'fakerphp/faker' => 
    array (
      'pretty_version' => 'v1.13.0',
      'version' => '1.13.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ab3f5364d01f2c2c16113442fb987d26e4004913',
    ),
    'fortawesome/font-awesome' => 
    array (
      'pretty_version' => '5.15.1',
      'version' => '5.15.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '57005cea6da7d1c67f3466974aecd25485f60452',
    ),
    'guzzlehttp/psr7' => 
    array (
      'pretty_version' => '1.x-dev',
      'version' => '1.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'f47ece9e6e8ce74e3be04bef47f46061dc18c095',
    ),
    'kartik-v/bootstrap-fileinput' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '5.1.x-dev',
      ),
      'reference' => '08c3dddb9dbcf268cd1d95e3b810e130e1319d68',
    ),
    'kartik-v/bootstrap-star-rating' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '4.0.x-dev',
      ),
      'reference' => '274395ae326a50ea01648e43879a01a256a072dd',
    ),
    'kartik-v/dependent-dropdown' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.4.x-dev',
      ),
      'reference' => 'c9e0b2b5b58ad717842396053804f8d0281df1ae',
    ),
    'kartik-v/php-date-formatter' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.3.x-dev',
      ),
      'reference' => '514a53660b0d69439236fd3cbc3f41512adb00a0',
    ),
    'kartik-v/yii2-bootstrap4-dropdown' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.0.x-dev',
      ),
      'reference' => '394cb4f85d82522ec5918f1581bdad518b324498',
    ),
    'kartik-v/yii2-builder' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.6.x-dev',
      ),
      'reference' => '74843d777dc9cb1d6e04f73f82c471b6aadde037',
    ),
    'kartik-v/yii2-datecontrol' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.9.x-dev',
      ),
      'reference' => '0e7057f644fb178c9a949836c2b0dc7325d07e02',
    ),
    'kartik-v/yii2-detail-view' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.8.x-dev',
      ),
      'reference' => '724f71394f8060da956e221cfb1ab26362d3ddde',
    ),
    'kartik-v/yii2-dialog' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.0.x-dev',
      ),
      'reference' => '6106d070695897ed4cf7a4c69897167bc6a76268',
    ),
    'kartik-v/yii2-grid' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '3.3.x-dev',
      ),
      'reference' => '1aa635faba7778d354a7a1e48c66b87744eed2af',
    ),
    'kartik-v/yii2-helpers' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.3.x-dev',
      ),
      'reference' => '1954c59dbd147af7aa791e172775036eb7ffa505',
    ),
    'kartik-v/yii2-icons' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.4.x-dev',
      ),
      'reference' => '6b3515a7021842b704b76ffab5bb80689233c85c',
    ),
    'kartik-v/yii2-krajee-base' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.0.x-dev',
      ),
      'reference' => '8aa48bb2e7613db45b3c34f7a9346d7f43b1f4ec',
    ),
    'kartik-v/yii2-widget-activeform' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.5.x-dev',
      ),
      'reference' => '4d578ff5508f1fc93ac8dd19cf22237216dfd5f9',
    ),
    'kartik-v/yii2-widget-affix' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '7f3266b1989ffd54a7025c4b1a322fcec38a9665',
    ),
    'kartik-v/yii2-widget-alert' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.1.x-dev',
      ),
      'reference' => '8be88744d2934eb0bfe701ec357fdfa8db96e3e9',
    ),
    'kartik-v/yii2-widget-colorinput' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.0.x-dev',
      ),
      'reference' => 'e35e6c7615a735b65557d6c38d112b77e2628c69',
    ),
    'kartik-v/yii2-widget-datepicker' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.4.x-dev',
      ),
      'reference' => '98257627814abf52aa8c1e6dc020d8eb6c1c39b5',
    ),
    'kartik-v/yii2-widget-datetimepicker' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.4.x-dev',
      ),
      'reference' => 'f64114bff520ea67f48e2a4e6372915968eb2c69',
    ),
    'kartik-v/yii2-widget-depdrop' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.0.x-dev',
      ),
      'reference' => '4dffe1fd0c9d9d5b7e6231f6cd059dfa36d22b3b',
    ),
    'kartik-v/yii2-widget-fileinput' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.1.x-dev',
      ),
      'reference' => 'e15d65047af23a3cf25e47e8c23444e750a27f2c',
    ),
    'kartik-v/yii2-widget-growl' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.1.x-dev',
      ),
      'reference' => 'af68935689073625ee578686ad4b96477d1cd526',
    ),
    'kartik-v/yii2-widget-rangeinput' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.0.x-dev',
      ),
      'reference' => 'c27f7c90299df5707fdd35ef2617042ce32b891c',
    ),
    'kartik-v/yii2-widget-rating' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.0.x-dev',
      ),
      'reference' => '91b92147d187674239bc644f809e02dee313eb81',
    ),
    'kartik-v/yii2-widget-select2' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.2.x-dev',
      ),
      'reference' => '05ed365ea5ed3555646db27dfb9beea79757a054',
    ),
    'kartik-v/yii2-widget-sidenav' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '54186e7f127dd5fc7ea3af0ac22d5f1ddd427a63',
    ),
    'kartik-v/yii2-widget-spinner' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.0.x-dev',
      ),
      'reference' => 'db22bd63fd1c1d3f9d8aa9477842f16338050ff1',
    ),
    'kartik-v/yii2-widget-switchinput' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '10afe2ac8274da9371b43aaf69e99fc553f1b947',
    ),
    'kartik-v/yii2-widget-timepicker' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.0.x-dev',
      ),
      'reference' => '27847bf0a3178d8ce526fe8a0f97832956ce8e45',
    ),
    'kartik-v/yii2-widget-touchspin' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.2.x-dev',
      ),
      'reference' => '221d764e129e7a8b9c1fe657cfc45e47489daa15',
    ),
    'kartik-v/yii2-widget-typeahead' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.0.x-dev',
      ),
      'reference' => '7b7041a3cbbeb2db0a608e9f6c9b3f4f63b0069d',
    ),
    'kartik-v/yii2-widgets' => 
    array (
      'pretty_version' => 'v3.4.1',
      'version' => '3.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e5a030d700243a90eccf96a070380bd3b76e17a3',
    ),
    'myclabs/deep-copy' => 
    array (
      'pretty_version' => '1.x-dev',
      'version' => '1.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '776f831124e9c62e1a2c601ecc52e776d8bb7220',
      'replaced' => 
      array (
        0 => '1.x-dev',
        1 => '9999999-dev',
      ),
    ),
    'opis/closure' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '3.6.x-dev',
      ),
      'reference' => '06b4961aabf900c72a20b7000bfa10fd7ae3035e',
    ),
    'phar-io/manifest' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.0.x-dev',
      ),
      'reference' => '85265efd3af7ba3ca4b2a2c34dbfc5788dd29133',
    ),
    'phar-io/version' => 
    array (
      'pretty_version' => '3.0.4',
      'version' => '3.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e4782611070e50613683d2b9a57730e9a3ba5451',
    ),
    'phpdocumentor/reflection-common' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.x-dev',
      ),
      'reference' => 'cf8df60735d98fd18070b7cab0019ba0831e219c',
    ),
    'phpdocumentor/reflection-docblock' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '5.x-dev',
      ),
      'reference' => 'e3324ecbde7319b0bbcf0fd7ca4af19469c38da9',
    ),
    'phpdocumentor/type-resolver' => 
    array (
      'pretty_version' => '1.x-dev',
      'version' => '1.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '6a467b8989322d92aa1c8bf2bebcc6e5c2ba55c0',
    ),
    'phpspec/php-diff' => 
    array (
      'pretty_version' => 'v1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fc1156187f9f6c8395886fe85ed88a0a245d72e9',
    ),
    'phpspec/prophecy' => 
    array (
      'pretty_version' => '1.12.2',
      'version' => '1.12.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '245710e971a030f42e08f4912863805570f23d39',
    ),
    'phpunit/php-code-coverage' => 
    array (
      'pretty_version' => '7.0.x-dev',
      'version' => '7.0.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'bb7c9a210c72e4709cdde67f8b7362f672f2225c',
    ),
    'phpunit/php-file-iterator' => 
    array (
      'pretty_version' => '2.0.x-dev',
      'version' => '2.0.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '4b49fb70f067272b659ef0174ff9ca40fdaa6357',
    ),
    'phpunit/php-text-template' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '31f8b717e51d9a2afca6c9f046f5d69fc27c8686',
    ),
    'phpunit/php-timer' => 
    array (
      'pretty_version' => '2.1.x-dev',
      'version' => '2.1.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '2454ae1765516d20c4ffe103d85a58a9a3bd5662',
    ),
    'phpunit/php-token-stream' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '4.0.x-dev',
      ),
      'reference' => '76fc0567751d177847112bd3e26e4890529c98da',
    ),
    'phpunit/phpunit' => 
    array (
      'pretty_version' => '8.5.x-dev',
      'version' => '8.5.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '957aaf33dcfb8f0173adb9da1199cffcb16311d8',
    ),
    'psr/container' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.1.x-dev',
      ),
      'reference' => '381524e8568e07f31d504a945b88556548c8c42e',
    ),
    'psr/event-dispatcher' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.0.x-dev',
      ),
      'reference' => '3ef040d7c05652f55bd05115baf059a445cfc79c',
    ),
    'psr/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.0.x-dev',
      ),
      'reference' => 'efd67d1dc14a7ef4fc4e518e7dee91c271d524e4',
    ),
    'psr/http-message-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'ralouphie/getallheaders' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '120b605dfeb996808c31b6477290a714d356e822',
    ),
    'rmrevin/yii2-fontawesome' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '5f296f4a72770d7be3472df025d4168f39a8861b',
    ),
    'sebastian/code-unit-reverse-lookup' => 
    array (
      'pretty_version' => '1.0.x-dev',
      'version' => '1.0.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '1de8cd5c010cb153fcd68b8d0f64606f523f7619',
    ),
    'sebastian/comparator' => 
    array (
      'pretty_version' => '3.0.x-dev',
      'version' => '3.0.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '1071dfcef776a57013124ff35e1fc41ccd294758',
    ),
    'sebastian/diff' => 
    array (
      'pretty_version' => '3.0.x-dev',
      'version' => '3.0.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '14f72dd46eaf2f2293cbe79c93cc0bc43161a211',
    ),
    'sebastian/environment' => 
    array (
      'pretty_version' => '4.2.x-dev',
      'version' => '4.2.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'd47bbbad83711771f167c72d4e3f25f7fcc1f8b0',
    ),
    'sebastian/exporter' => 
    array (
      'pretty_version' => '3.1.x-dev',
      'version' => '3.1.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '6b853149eab67d4da22291d36f5b0631c0fd856e',
    ),
    'sebastian/global-state' => 
    array (
      'pretty_version' => '3.0.x-dev',
      'version' => '3.0.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '474fb9edb7ab891665d3bfc6317f42a0a150454b',
    ),
    'sebastian/object-enumerator' => 
    array (
      'pretty_version' => '3.0.x-dev',
      'version' => '3.0.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'e67f6d32ebd0c749cf9d1dbd9f226c727043cdf2',
    ),
    'sebastian/object-reflector' => 
    array (
      'pretty_version' => '1.1.x-dev',
      'version' => '1.1.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '9b8772b9cbd456ab45d4a598d2dd1a1bced6363d',
    ),
    'sebastian/recursion-context' => 
    array (
      'pretty_version' => '3.0.x-dev',
      'version' => '3.0.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '367dcba38d6e1977be014dc4b22f47a484dac7fb',
    ),
    'sebastian/resource-operations' => 
    array (
      'pretty_version' => '2.0.x-dev',
      'version' => '2.0.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '31d35ca87926450c44eae7e2611d45a7a65ea8b3',
    ),
    'sebastian/type' => 
    array (
      'pretty_version' => '1.1.x-dev',
      'version' => '1.1.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '0150cfbc4495ed2df3872fb31b26781e4e077eb4',
    ),
    'sebastian/version' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '99732be0ddb3361e16ad77b68ba41efc8e979019',
    ),
    'select2/select2' => 
    array (
      'pretty_version' => 'dev-develop',
      'version' => 'dev-develop',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '3df83c83b56ba0886afbe55c64b49d6c2fd39298',
    ),
    'swiftmailer/swiftmailer' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '6.2.x-dev',
      ),
      'reference' => '59935da5e9d4c7ae801bb393d638dac2515498c9',
    ),
    'symfony/browser-kit' => 
    array (
      'pretty_version' => 'v4.2.4',
      'version' => '4.2.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '61d85c5af2fc058014c7c89504c3944e73a086f0',
    ),
    'symfony/console' => 
    array (
      'pretty_version' => '5.x-dev',
      'version' => '5.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '5a8ebe413fbd4091d5b3c5b70e298141681c59c7',
    ),
    'symfony/css-selector' => 
    array (
      'pretty_version' => '5.x-dev',
      'version' => '5.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => 'cae52f750588574e51c51dabc08a8903ee5c7ffc',
    ),
    'symfony/deprecation-contracts' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '2.3.x-dev',
      ),
      'reference' => '366d03137004f7fd0e9d9ae9360eaf2d5c66d1ad',
    ),
    'symfony/dom-crawler' => 
    array (
      'pretty_version' => '4.4.x-dev',
      'version' => '4.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '349818b3b42c6adf48c4d2770b1bb7e56711d750',
    ),
    'symfony/event-dispatcher' => 
    array (
      'pretty_version' => '5.x-dev',
      'version' => '5.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '6d0ca87886dc1433bea9f938db3bf9d92a6ccf63',
    ),
    'symfony/event-dispatcher-contracts' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '2.3.x-dev',
      ),
      'reference' => 'f4680856fc4416885ac312e709137afddb1077cc',
    ),
    'symfony/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '2.0',
      ),
    ),
    'symfony/finder' => 
    array (
      'pretty_version' => '5.x-dev',
      'version' => '5.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '9e322de6efc01ed994745547dfee422c2725b06e',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '1.21.x-dev',
      ),
      'reference' => '7130f348df2f842044038aaae9d6653dc9d67649',
    ),
    'symfony/polyfill-iconv' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '1.21.x-dev',
      ),
      'reference' => 'b95289d09c51119e2c4dabd59370c062733dc6f3',
    ),
    'symfony/polyfill-intl-grapheme' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '1.21.x-dev',
      ),
      'reference' => 'e314d4992832c3a0a68ca731fadd959917320fda',
    ),
    'symfony/polyfill-intl-idn' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '1.21.x-dev',
      ),
      'reference' => '947d45e18c48ae1ccce23ff1add57ff4b0c031cd',
    ),
    'symfony/polyfill-intl-normalizer' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '1.21.x-dev',
      ),
      'reference' => '3a79a2226897adae0cab81688fbc5144e2fc53f6',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '1.21.x-dev',
      ),
      'reference' => 'de14691dc88bbbc5535de7f0e32080977dc1d23f',
    ),
    'symfony/polyfill-php72' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '1.21.x-dev',
      ),
      'reference' => '4a4465f57b476085b62e74087f74ae2e753ff633',
    ),
    'symfony/polyfill-php73' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '1.21.x-dev',
      ),
      'reference' => '8c0d39c1526009b97f43beea4cc685bbc353a70b',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '1.21.x-dev',
      ),
      'reference' => '54cc82c30ba7ed02bc64f5d010488c159b5f1706',
    ),
    'symfony/service-contracts' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '2.3.x-dev',
      ),
      'reference' => 'e0d43e6e2f909287d2e4e867ca5c131a661f08ef',
    ),
    'symfony/string' => 
    array (
      'pretty_version' => '5.x-dev',
      'version' => '5.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '99f25957efe05db14a1aa6cff643eca0f83a952c',
    ),
    'symfony/yaml' => 
    array (
      'pretty_version' => '5.x-dev',
      'version' => '5.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '861d4af84c695aef0ec7475455f032a00a87cc0a',
    ),
    'theseer/tokenizer' => 
    array (
      'pretty_version' => '1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '75a63c33a8577608444246075ea0af0d052e452a',
    ),
    'warrence/yii2-kartikgii' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '82a9da2e24e7ee70209ee81dbe0a53902b16039b',
    ),
    'webmozart/assert' => 
    array (
      'pretty_version' => '1.9.1',
      'version' => '1.9.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bafc69caeb4d49c39fd0779086c03a3738cbb389',
    ),
    'yiisoft/yii2' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.0.x-dev',
      ),
      'reference' => '281dc5fb95bf747828798024c10bc6171d2f4ae3',
    ),
    'yiisoft/yii2-app-basic' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => 'c8fa324a4d3817a03f0b110ed739764cf2c9ffd1',
    ),
    'yiisoft/yii2-bootstrap' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.0.x-dev',
      ),
      'reference' => 'c45bf125671f3f6237a605241486a6dd35b142a7',
    ),
    'yiisoft/yii2-composer' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.0.x-dev',
      ),
      'reference' => 'b994e9678e34c2d17008c42a29fedb29d9ebf80b',
    ),
    'yiisoft/yii2-debug' => 
    array (
      'pretty_version' => '2.1.16',
      'version' => '2.1.16.0',
      'aliases' => 
      array (
      ),
      'reference' => '0d8ce76b2dd036a5fc38b26434e1c672ad8975a9',
    ),
    'yiisoft/yii2-faker' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.0.x-dev',
      ),
      'reference' => '3f021347a97ee4cd6826a208a657182918053341',
    ),
    'yiisoft/yii2-gii' => 
    array (
      'pretty_version' => '2.1.4',
      'version' => '2.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd879cb186361fbc6f71a2d994d580b5a071a5642',
    ),
    'yiisoft/yii2-jui' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.0.x-dev',
      ),
      'reference' => '739d23f2a63c3ee6994583466426c39e7a67bf46',
    ),
    'yiisoft/yii2-swiftmailer' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.1.x-dev',
      ),
      'reference' => 'ea7a13455c8381b4690ba1cea6c8cf41c9388f98',
    ),
  ),
);
